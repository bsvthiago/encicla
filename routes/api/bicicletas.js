var express = require('express');
var router = express.Router();
var controllerAPI = require('../../controllers/api/biciControllerAPI');

router.get('/', controllerAPI.bicicleta_list);
router.get('/:id', controllerAPI.bicicleta_item);
router.post('/create', controllerAPI.bicicleta_create);
router.post('/delete', controllerAPI.bicicleta_delete);
router.post('/update', controllerAPI.bicicleta_update);


module.exports = router;