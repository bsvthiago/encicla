Para iniciar el proyecto requiere ejecutar los node module dentro de carpeta raiz ( npm install)
dependencias:
*  npm install pug -g
*  npm install express -g
*  npm install -g express-generator

Luego en su carpeta raiz abrir terminal para ejecutar el servidor local con el comando
" npm start "  o "npm run devstart"
En dicho proyecto iniciando en su navegador en al direccion:  " localhost:3000/ "  directamente van a poder encontrar la página principal.

Iniciando en:  localhost:3000/welcome van a poder observar el mensaje "Welcome to Express" conservado
del index original de cuando se inició el proyecto de express. Este index fue reubicado siguiendo 
la lógica requerida del proyecto.

Iniciando en:  localhost:3000/bicicletas van a poder observar la lista de bicicletas en memoria, y van a
poder realizar el ABM de las mismas.

Iniciando en localhost:3000/api/bicicletas van a poder observar la lista de bicicletas en memoria formato .JSON
donde puedes utilizar la herramienta Postman o hacer tus propios end points desde
  localhost:3000/bicicletas/create (GET,POST, PUT etc.).

Santiago Ospina.
JavaScript Dev.