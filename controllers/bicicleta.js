var bicicleta = require('../models/bicicleta');

//controlador del index
exports.bicicleta_list = function(req, res){
    res.render('bicicletas/index', {bicis: bicicleta.todas});
}

//controlador de create (ver la página para crear)
exports.bicicleta_create_get = function(req, res){
    res.render('bicicletas/create');
}

//controlador de create (para cuando se agrega la bicicleta)
exports.bicicleta_create_post = function(req, res){
    var bici = new bicicleta(req.body.id, req.body.color, req.body.modelo);
    bici.ubicacion = [req.body.lat, req.body.long];
    bicicleta.add(bici);
    res.redirect('/bicicletas');
}

//controlador de update (ver el formulario)
exports.bicicleta_update_get = function(req, res){
    var bici = bicicleta.findById(req.params.id);
    res.render('bicicletas/update', {bici});
}

//controlador de update (para enviar los datos modificados)
exports.bicicleta_update_post = function(req, res){
    var bici = bicicleta.findById(req.params.id);
    bici.id = req.body.id;
    bici.modelo = req.body.modelo;
    bici.color = req.body.color;
    bici.ubicacion = [req.body.lat, req.body.long];
    res.redirect('/bicicletas');
}

//controlador de remove (para cuando se quiere eliminar una bicicleta)
exports.bicicleta_delete_post = function(req, res){
    bicicleta.removeById(req.body.id)
    res.redirect('/bicicletas');
}

//controlador de item (ver una bicicleta)
exports.bicicleta_item = function(req, res){
    var bici = bicicleta.findById(req.params.id);
    res.render('bicicletas/item', {bici});
}