var bicicleta = require('../../models/bicicleta');

exports.bicicleta_list = function(req, res){
    res.status(200).json({
        bicicletas: bicicleta.todas
    });
}

exports.bicicleta_create = function(req, res){
    var bici = new bicicleta(req.body.id, req.body.color, req.body.modelo);
    bici.ubicacion = [req.body.lat, req.body.long];
    bicicleta.add(bici);
    res.status(200).json({
        bicicletas: bicicleta.todas
        // o tambien puede devolver solamente la nueva bicicleta añadida:
        // bicicleta: bici
    });
}

exports.bicicleta_delete = function(req, res){
  bicicleta.removeById(req.body.id);
  res.status(204).send();
  //se usa el 204 porque no se devuelve nada
}

exports.bicicleta_update = function(req, res){
    var bici = bicicleta.findById(req.body.id);
    bici.id = req.body.id;
    bici.modelo = req.body.modelo;
    bici.color = req.body.color;
    bici.ubicacion = [req.body.lat, req.body.long];
    res.status(200).json({
        bicicletas: bicicleta.todas
        // o tambien puede devolver solamente la nueva bicicleta añadida:
        // bicicleta: bici
        //Falta definir el error cuando no se encuentra el id de bici
    });
}

exports.bicicleta_item = function(req, res){
    var bici = bicicleta.findById(req.params.id);
    console.log('paso')
    res.status(200).json({bici});
}