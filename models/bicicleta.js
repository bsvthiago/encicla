var bici = function(id, color, modelo, ubicacion){
    this.id = id;
    this.color = color;
    this.modelo = modelo;
    this.ubicacion = ubicacion;
}

//defino un método para devolver la información como string
bici.prototype.toString = function(){
    return 'id: ' + this.id + ', color: ' + this.color;
}

//defino un método para ver todas las bicis
bici.todas = [];

//defino un método para agregar una bici al arreglo que contiene todas
bici.add = function(unaBici){
    bici.todas.push(unaBici);
}

//defino un método para buscar una bici en particular
bici.findById = function(biciId){
    var unaBici = bici.todas.find(x => x.id == biciId);
    if (unaBici) { 
        return unaBici
    } else { throw new Error(`No existe una bicicleta con el id ${biciId}`); }
}

//defino un método para remover una bici en particular
bici.removeById = function(biciId){
    //var unaBici = bici.todas.findById(biciId); podría buscarlo solamente para asegurarme que exista
    for (let i = 0; i < bici.todas.length; i++) {
        if (bici.todas[i].id == biciId) {
            bici.todas.splice(i, 1);
            break;            
        }
    }
}

var bici1 = new bici(1,'verde','montaña', [6.23575,-75.57958]);
var bici2 = new bici(2,'rojo','deportiva', [6.27088,-75.57898]);
var bici3 = new bici(3,'azul','urbana', [6.2837,-75.5664]);
var bici4 = new bici(4,'negro','deportiva', [6.26776,-75.57052]);
var bici5 = new bici(5,'negro','deportiva', [6.2502,-75.5925]);

bici.add(bici1);
bici.add(bici2);
bici.add(bici3);
bici.add(bici4);
bici.add(bici5);

module.exports = bici;